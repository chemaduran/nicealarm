package com.chemaduran.nicealarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/**
 * Created by IntelliJ IDEA.
 * User: Jose
 * Date: 6/02/12
 * Time: 9:25
 * To change this template use File | Settings | File Templates.
 */
public abstract class TtsProviderFactory extends Activity {

    public abstract void init(Context context, String texto_alarma);

    public abstract void shutdown();

    private static TtsProviderFactory sInstance;

    public abstract void sayThis(String text);

    protected abstract void onActivityResult(Context context, int requestCode, int resultCode, Intent data);

    public static TtsProviderFactory getInstance() {
        if (sInstance == null) {
            int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
            if (sdkVersion < Build.VERSION_CODES.DONUT) {
                return null;
            }

            try {
                String className = "TtsProviderImpl";
                Class<? extends TtsProviderFactory> clazz =
                        Class.forName(TtsProviderFactory.class.getPackage().getName() + "." + className)
                                .asSubclass(TtsProviderFactory.class);
                sInstance = clazz.newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return sInstance;
    }
    
    public static boolean isInstantiated()
    {
        if (sInstance == null)
            return false;
        else
            return true;

    }



}
