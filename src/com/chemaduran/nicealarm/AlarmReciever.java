package com.chemaduran.nicealarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;


public class AlarmReciever extends BroadcastReceiver   {

    @Override
    public void onReceive(Context ctx, Intent arg1) {
        PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock( PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE | PowerManager.SCREEN_DIM_WAKE_LOCK, "AppAlarmReceiver");
        wl.acquire(120000);
        Intent i = new Intent(ctx, AalService.class);
        i.setAction(AalService.ACTION_LAUNCH_ALARM);
        ctx.startService(i);
    }

}
