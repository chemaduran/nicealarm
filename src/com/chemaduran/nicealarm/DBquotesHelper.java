package com.chemaduran.nicealarm;

import android.content.Context;
import android.content.res.Resources;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;



/**
 * Created by IntelliJ IDEA.
 * User: Jose
 * Date: 10/02/12
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */
public class DBquotesHelper extends SQLiteOpenHelper {

    //Sentencia SQL para crear la tabla de Usuarios
    String sqlCreate = "CREATE TABLE Citas (categoria TEXT, cita TEXT)";

    private static Context mCtx;
    private static String TAG = "CHEMA_D: DBquotesHelper";

    public DBquotesHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        mCtx = context;

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //To change body of implemented methods use File | Settings | File Templates.
        //Se ejecuta la sentencia SQL de creación de la tabla
        sqLiteDatabase.execSQL(sqlCreate);

        Log.d(TAG, "onCreate init");

        read_and_insert_db(sqLiteDatabase);

        Log.d(TAG, "onCreate exit");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //To change body of implemented methods use File | Settings | File Templates.
        //NOTA: Por simplicidad del ejemplo aquí utilizamos directamente la opción de
        //      eliminar la tabla anterior y crearla de nuevo vacía con el nuevo formato.
        //      Sin embargo lo normal será que haya que migrar datos de la tabla antigua
        //      a la nueva, por lo que este método debería ser más elaborado.

        Log.d(TAG, "onUpgrade init");

        //Se elimina la versión anterior de la tabla
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Citas");

        //Se crea la nueva versión de la tabla
        sqLiteDatabase.execSQL(sqlCreate);

        read_and_insert_db(sqLiteDatabase);

        Log.d(TAG, "onUpgrade exit");
    }

    public String getFileContent(Resources resources, int rawId) throws
            IOException
    {
        Log.d(TAG, "getFileContent init");
        InputStream is = resources.openRawResource(rawId);
        int size = is.available();
        // Read the entire asset into a local byte buffer.
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        
        String result = new String(buffer);
        //result = DatabaseUtils.sqlEscapeString(result);
        // Convert the buffer into a string.
        Log.d(TAG, "getFileContent exit");
        return result;
    }

    public void read_and_insert_db (SQLiteDatabase sqLiteDatabase)
    {
        Log.d(TAG, "read_and_insert_db init");

        try {
            String sqlCode = getFileContent(mCtx.getResources(), R.raw.citas);
            for (String sqlStatements : sqlCode.split(";\n"))
            {
                Log.v(TAG, sqlStatements);
                //DatabaseUtils.sqlEscapeString(sqlStatements);
                sqLiteDatabase.execSQL(sqlStatements);
            }

            Log.d(TAG, "Creating database done.");
        } catch (IOException e) {
            // Should never happen!
            Log.d(TAG, "Error reading sql file " + e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (SQLiteException e)
        {
            Log.d(TAG, "Error executing sql sentence: " + e.getMessage(), e);
        }


        Log.d(TAG, "read_and_insert_db exit");

    }




}
