package com.chemaduran.nicealarm;

import android.*;
import android.R;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: Jose
 * Date: 6/02/12
 * Time: 9:25
 * To change this template use File | Settings | File Templates.
 */
public class TtsProviderImpl extends TtsProviderFactory implements TextToSpeech.OnInitListener {

    private TextToSpeech tts;
    //status check code
    private int MY_DATA_CHECK_CODE = 1234;      // TODO: check TTS available
    private String TAG = "CHEMA_D: TtsProviderImpl";
    private String txtAlarma = "";


    public void init(Context context, String texto_alarma) {
        Log.d(TAG, "Entering init");

        txtAlarma = texto_alarma;

        if (tts == null) {
            Log.d(TAG, "Entering TTS new");
            tts = new TextToSpeech(context, this);
        }
        else
            sayThis(texto_alarma);



        Log.d(TAG, "Exiting init");
    }

    @Override
    public void onInit(int status) {
        Log.d(TAG, "Entering OnInit");
        Locale loc = new Locale("es", "", "");
        if (tts.isLanguageAvailable(loc) >= TextToSpeech.LANG_AVAILABLE)
        {
            Log.d(TAG, "Locale set");
            tts.setLanguage(loc);
        }
        else if (status == TextToSpeech.ERROR)
        {
            Toast.makeText(this, "No se pudo inicializar el TTS", Toast.LENGTH_LONG).show();
        }

        tts.speak(txtAlarma, TextToSpeech.QUEUE_FLUSH, null);
        Log.d(TAG, "Exiting OnInit");
    }

    @Override
    public void shutdown() {
        Log.d(TAG, "Entering shutdown");
        tts.stop();
        tts.shutdown();
        Log.d(TAG, "Exiting shutdown");
    }

    @Override
    protected void onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void sayThis(String text)
    {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


}

