package com.chemaduran.nicealarm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: Jose
 * Date: 10/02/12
 * Time: 12:24
 * To change this template use File | Settings | File Templates.
 */
public class TTS extends Activity implements TextToSpeech.OnInitListener {

    //TTS object
    private TextToSpeech myTTS;
    //status check code
    private int MY_DATA_CHECK_CODE = 1234;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //check for TTS data
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);

    }

    //speak the user text
    public void speakWords(String speech) {
        //speak straight away
        myTTS.speak(speech , TextToSpeech.QUEUE_FLUSH, null);
    }


    //act on result of TTS data check
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MY_DATA_CHECK_CODE)
        {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS)
            {
                //the user has the necessary data - create the TTS
                myTTS = new TextToSpeech(this, this);
            }
            else
            {
                //no data - install it now
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }



    public void onInit(int initStatus) {

        //check for successful instantiation
        if (initStatus == TextToSpeech.SUCCESS)
        {
            if(myTTS.isLanguageAvailable(Locale.US)==TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.US);
        }
        else if (initStatus == TextToSpeech.ERROR)
        {
            //Toast.makeText(this, "Vemos", Toast.LENGTH_LONG).show();
        }


        //Recuperamos la informaciÃ³n pasada en el intent
        Bundle bundle = this.getIntent().getExtras();

        //Construimos el mensaje a mostrar
        myTTS.speak(bundle.getString("TEXTO_ALARMA") , TextToSpeech.QUEUE_FLUSH, null);

    }

    protected void onDestroy() {
       myTTS.stop();
       myTTS.shutdown();
       // The activity is about to be destroyed.
       super.onDestroy();

    }


}
